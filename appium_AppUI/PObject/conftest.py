#!/usr/bin/python3
# -*- coding: utf-8 -*-
# @Time    : 2021/9/23 23:40
# @Author  : muchenmglin
import logging

logging.basicConfig(level=logging.INFO,
                    format='%(asctime)s %(filename)s[line:%(lineno)d] %(levelname)s %(message)s',
                    datefmt='%a, %d %b %Y %H:%M:%S',
                    filename='log.text',
                    filemode='w')