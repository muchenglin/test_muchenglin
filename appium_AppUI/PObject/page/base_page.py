#!/usr/bin/python3
# -*- coding: utf-8 -*-
# @Time    : 2021/9/22 23:28
# @Author  : muchenmglin
# 完成基本内容的封装，例子：初始化driver ，find
import logging

from appium.webdriver.webdriver import WebDriver

logger = logging.getLogger()

class BasePage():

    def __init__(self,driver: WebDriver=None):
        self.driver = driver

    def find(self, by, value):
        logger.info("find")
        logger.info(by)
        logger.info(value)
        return self.driver.find_element(by, value)

    def find_and_click(self, by, value):
        logger.info("find_and_click")

        self.find(by, value).click()

    def find_and_send(self, by, value, text):
        logger.info("find_and_send")
        self.find(by,value).send_keys(text)

    def back(self,num=3):
        logger.info("back")
        for i in range(0,num):
            self.driver.back()
    # 滑动点击添加联系人
    def find_uiautomator(self):
        logger.info("uiautomator")
        self.driver.find_element_by_android_uiautomator(('new UiScrollable(new UiSelector().'
                                                         'scrollable(true).instance(0)).'
                                                         'scrollIntoView(new UiSelector().text("添加成员").'
                                                         'instance(0))')).click()
