#!/usr/bin/python3
# -*- coding: utf-8 -*-
# @Time    : 2021/9/25 23:49
# @Author  : muchenmglin
from appium.webdriver.common.mobileby import MobileBy

from App.PObject.page.base_page import BasePage
from App.PObject.page.delmember_page import DelmemberPage


class EditProfilePage(BasePage):
    edit_user_element = (MobileBy.XPATH, "//*[@text='编辑成员']")

    def edit_user(self):
        # self.driver.find_element(MobileBy.XPATH, "//*[@text='编辑成员']").click()
        self.find_and_click(self.edit_user_element)
        return DelmemberPage()