#!/usr/bin/python3
# -*- coding: utf-8 -*-
# @Time    : 2021/9/21 23:06
# @Author  : muchenmglin
import os

from App.PObject.page.base_page import BasePage
from App.PObject.page.main_page import MainPage
from appium import webdriver

class App(BasePage):
    # app.py   存放APP相关得操作, 打开，重启，关闭
    def start(self):
        # udid = os.getenv("udid")
        # port = os.getenv("port")
        port = "4723"
        if self.driver == None:
            print('driver is None')
        # 创建json字符串连接设备
            caps = {}
            caps["platformName"] = "android"
            caps["deviceName"] = "127.0.0.1:7555"
            caps["appPackage"] = "com.tencent.wework"
            caps["appActivity"] = ".launch.LaunchSplashActivity"
            caps["noReset"] = True
            caps["ensureWebviewsHavePages"] = True
            # caps['udid'] = udid
            # 完成客户端与服务端的连接，创建driver
            self.driver = webdriver.Remote(f"http://127.0.0.1:{port}/wd/hub", caps)
            self.driver.implicitly_wait(5)
        else:
            print('driver is not None')
            # launch_app为热启动
            self.driver.launch_app()
        return self

    def restart(self):
        pass


    def stop(self):
        # 销毁driver
        self.driver.quit()

    def goto_main(self):
        # APP入口
        return MainPage(self.driver)