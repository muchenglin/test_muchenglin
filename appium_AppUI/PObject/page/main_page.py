#!/usr/bin/python3
# -*- coding: utf-8 -*-
# @Time    : 2021/9/21 23:09
# @Author  : muchenmglin
from appium.webdriver.common.mobileby import MobileBy
from appium.webdriver.webdriver import WebDriver
from App.PObject.page.addresslist_page import AddressListPage
from App.PObject.page.base_page import BasePage
from App.PObject.page.profile_page import ProfilePage


class MainPage(BasePage):
    # 命名一个属性
    addresslist_element = (MobileBy.XPATH, "//*[@text='通讯录']")
    user_ele = (MobileBy.XPATH,"//*[@text='段雷']")

    def goto_addresslist(self):
        # 点击跳转通讯录
        self.find_and_click(*self.addresslist_element) #解元组
        # self.find_and_click(MobileBy.XPATH, "//*[@text='通讯录']")
        return AddressListPage(self.driver)

    def goto_profile(self):
        self.find_and_click(*self.addresslist_element)
        self.find_and_click(self.user_ele)
        # 跳转进入prfile页面
        return ProfilePage()