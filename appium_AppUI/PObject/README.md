####封装PO
page/  存放PAGE页面
    app.py   存放APP相关得操作,打开，重启，关闭
case/  存放测试用例


######流程
1、创建PO
2、编写测试用例，跑通业务流程，减少调试时间
3、填充PO业务内容，将每个页面需要的包导入，把每个页面的driver传递下去

####### 封装BasePage的作用
page/
      base_page.py  存放初始化driver，封装find_by_element...常用的方法
1、封装重复代码，便于维护测试用例
2、避免driver赋予其他值导致代码运行错误

######driver 的复用
每次执行测试用例，需要初始化一次driver
预期：
--driver 存在，不为none  复用这个driver
--driver 不存在，为none，重新创建一个driver
在start 方法中复用driver


'''shell
set udid="192.168.57.103:5555" port=4723 
pytest test_contact.py
'''
