#!/usr/bin/python3
# -*- coding: utf-8 -*-
# @Time    : 2021/6/6 9:57 下午
# @Author  : muchenglin
# @File    : test_calc.py
# @Software: PyCharm
import allure
import pytest
import yaml
import logging

from PytestClass.calc import Calculator

# 解析yaml文件获取测试数据
with open('./datas/calc.yaml') as f:
    datas = yaml.safe_load(f)['add']
    add_datas = datas['add_datas']
    sub_datas = datas['duv_datas']
    add_id = datas['add_id']
    sub_id = datas['duv_id']


logging.basicConfig(level=logging.INFO)
logger=logging.getLogger()

@allure.feature("测试计算器")
class TestCalc:
    @allure.title("测试相加_{a}_{b}_{expect}")
    @pytest.mark.parametrize(
        "a,b,expect",
        add_datas,
        ids=add_id)
    @allure.story("加法运算")
    def test_add(self,get_calc,a,b,expect):
        # 打印测试用例日志
        with allure.step("打印日志"):
            logger.info(f"相加:{a}+{b} = {expect}")
        #调用add方法
        result = get_calc.add(a,b)
        # 判断计算结果是否为小数，如果为小数则保留两位小数
        with allure.step("判断计算结果是否为小数，如果为小数则保留两位小数"):
            if isinstance(result,float):
                result=round(result,2)
        #得到结果之后，需要添加断言
        with allure.step("断言"):
            assert result == expect

    @allure.title("测试相除_{a}_{b}_{expect}")
    @pytest.mark.parametrize(
        "a,b,expect",
        sub_datas,
        ids=sub_id
    )
    @allure.story("除法运算")
    def test_duv(self,get_calc,a,b,expect):
        with allure.step("打印日志"):
            logger.info(f"相除：{a} / {b} ={expect}")

        result = get_calc.duv(a,b)
        with allure.step("判断计算结果是否为小数，如果为小数则保留两位小数"):
            if isinstance(result,float):
                result=round(result,2)
        #得到结果之后，需要添加断言
        with allure.step("断言"):
            assert result == expect
