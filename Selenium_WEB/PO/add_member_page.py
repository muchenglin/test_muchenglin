#!/usr/bin/python3
# -*- coding: utf-8 -*-
# @Time    : 2021/7/11 10:05 下午
# @Author  : muchenglin
# @Software: PyCharm
from time import sleep

from selenium.webdriver.common.by import By

from Weixin_web.po.BasePage import BasePage



class AddMemberPage(BasePage):
    # 添加成员页面
    _base_url = "https://work.weixin.qq.com/wework_admin/frame#contacts"
    username_ele = (By.ID, "username")
    # 添加成员方法
    def add_member(self,name):
        '''
        添加通讯录成员
        :return:
        '''
        from Weixin_web.po.contact_page import ContactPage
        # 添加姓名
        # self.driver.find_element(By.ID, "username").send_keys("食物计划书")
        self.find(*self.username_ele).send_keys(name)
        # 添加账号
        self.find(By.ID, "memberAdd_acctid").send_keys(acctid)
        # 添加手机号
        self.find(By.ID, "memberAdd_phone").send_keys(phone)
        # 点击保存按钮
        # self.driver.find_element(By.CSS_SELECTOR, "qui_btn ww_btn js_btn_save").click()
        self.find(By.CSS_SELECTOR, ".js_btn_save").click()
        return ContactPage(self.driver)
    # 添加成员失败
    def add_member_fail(self,name):
        '''
        添加通讯录成员
        :return:
        '''
        from Weixin_web.po.contact_page import ContactPage
        # 添加姓名
        # self.driver.find_element(By.ID, "username").send_keys("食物计划书")
        self.find(*self.username_ele).send_keys("十六")
        # 添加账号
        self.find(By.ID, "memberAdd_acctid").send_keys("456")
        # 添加手机号
        self.find(By.ID, "memberAdd_phone").send_keys("12345678900")
        # 点击保存按钮按钮
        # self.driver.find_element(By.CSS_SELECTOR, "qui_btn ww_btn js_btn_save").click()
        self.find(By.CSS_SELECTOR, ".js_btn_save").click()
        # 点击取消按钮
        self.find(By.CSS_SELECTOR,".js_btn_cancel").click()
        return ContactPage(self.driver)